# Laboration 3

## Environment & Tools Windows 10  

Eclipse IDE 2020-09  

JDK 15.0.1  

Git version 2.26.2.windows.1  

## Purpose  

The purpose is to learn the syntax of Java by making a tiny application that takes user input and uses that input.  

## Procedures  

Once I understood how the classes were connected in the same package the laboration was very easy. The only issue I had was that whenever I entered an input and pressed enter, I sometimes had to press enter twice, but after some trials and errors I managed to solve it.  

## Discussion
The laboration was very easy but a good way to get into the new syntax and the way classes are connected.