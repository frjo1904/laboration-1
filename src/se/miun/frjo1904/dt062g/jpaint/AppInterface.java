package se.miun.frjo1904.dt062g.jpaint;
import java.util.*;

/**
* <h1>Assignment 1</h1>
* <p>This application allows the user to enter data for a circle
* or rectangle. The circumference and area are then calculated
* and the result is displayed to the standard output.</p>
* 
* @author Fredrik Josefsson (frjo1904)
* @version 1.0
* @since 2020-11-04
*/

public class AppInterface {
		private Rectangle rectangle = new Rectangle();
		private Circle circle = new Circle();
		
		private void showMenu() {
			System.out.println("Select option:");
			System.out.println("Type 'rectangle' to choose rectangle.");
			System.out.println("Type 'circle' to circle.");
			System.out.println("Type 'exit' to exit application.");
		
		}
		
		private String userChoice() {
			Scanner input = new Scanner(System.in);
			String answer = input.nextLine();
			return answer;
		}
		
		private void inputRectangle() {
			Scanner input = new Scanner(System.in);
			System.out.println("Enter height of rectangle: ");
			
	
			while(!input.hasNextInt()) {
				System.out.println("Invalid input. Try again.");
				input.nextLine();
			}
			int height = input.nextInt();
		
			
			System.out.println("Enter length of rectangle: ");
			int length;
			while(!input.hasNextInt()) {
				System.out.println("Invalid input. Try again.");
				input.nextLine();
			}
			length = input.nextInt();
			rectangle.setHeight(height);
			rectangle.setLength(length);
		
		}
		private void inputCircle() {
			Scanner input = new Scanner(System.in);
			
			System.out.println("Enter radius of circle: ");
			double radius;
			while(!input.hasNextDouble()) {
				System.out.println("Invalid input. Try again.");
				input.nextLine();
			}
			radius = input.nextDouble();
			circle.setRadius(radius);
		}
		
		public void run() {
			boolean again = true;
			while(again) {
				showMenu();
				String input = userChoice();
				
				if(input.equals("rectangle")) {
					inputRectangle();
					System.out.println("Area: " + rectangle.calculateArea());
					System.out.println("Perimeter: " + rectangle.calculatePerimeter() + '\n');
				}
				else if(input.equals("circle")) {
					inputCircle();
					System.out.println("Area: " + circle.calculateArea());
					System.out.println("Circumference: " +  circle.calculateCircumference() + '\n');
				}
				else if(input.equals("exit")) {
				
					return;
				}
				else {
					System.out.println("Invalid input. Please try again" + '\n');
				}
				
			}
		
	}

}
