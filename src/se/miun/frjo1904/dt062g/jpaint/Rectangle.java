package se.miun.frjo1904.dt062g.jpaint;

public class Rectangle {
	private int length;
	private int height;

	public Rectangle(){}
	
	
	public int calculatePerimeter() {
		 int perimeter = (length*2) + (height*2);
		 return perimeter;
	}
	public int calculateArea() {
		int area = length * height;
		return area;
	}
	public void setLength(int aLength) {
		length = aLength;
	}
	public void setHeight(int aHeight) {
		height = aHeight;
	}
	
	
}
