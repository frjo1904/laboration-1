package se.miun.frjo1904.dt062g.jpaint;

public class Circle {
private double radius;
private final double PI = 3.14;

public Circle() {}

public double calculateCircumference() {
	double circumference = (radius*2) * PI;
	return circumference;
}

public double calculateArea() {
	double area = radius*radius*PI;
	return area;
}

public void setRadius(double aRadius) {
	radius = aRadius;
}

}
